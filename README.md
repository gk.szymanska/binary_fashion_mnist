## binary_fashion_MNIST

Both file ("binary_fashion_MNIST_task_1" and "binary_fashion_MNIST_task_2") have to be located in root folder with folder "Shoes" which were sended to me by mail.

To run code given libraries are needed: tensorflow, sklearn, itertools, numpy, matplotlib, os, tqdm, cv2, random. 

In root folder after proceed "binary_fashion_MNIST_task_1" the file with model will be saved and later used by "binary_fashion_MNIST_task_2". 

About tasks: 

"binary_fashion_MNIST_task_1" - in this file keras model is building, then training on 'fashion_MNIST dataset' where labels were customized to binary recognision 'shoes' and 'not-shoes', model is saved. Proposed model was chosen from ather models with different hyperparameters andnumbers of layers, because of stable loss function (without oscilation), quite good accuracy, not overfitting. 

"binary_fashion_MNIST_task_2" - in this file model from previous task is used as pretrained model. This time to train new model 1 image per class is given. Because of this no new layers has been added (too much random and not enough data to get betetr weights) and first 6 classes are not trainable (reason: very small train dataset, after a lot of tries this number of nontrainable layer gives generaly the best -but still not so good- results). 

"binary_fashion_MNIST_task_2_data_augumentation" - similar to "binary_fashion_MNIST_task_2" but instead of training only on identical image, I use data augumentation tool to make dataset little bigger. Results still depends very much on random. 
