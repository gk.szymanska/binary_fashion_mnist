import numpy as np
import tensorflow as tf
from tensorflow.keras import Model
from tensorflow.keras.layers import Dense, Dropout, Flatten
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import matplotlib.pyplot as plt
from tqdm import tqdm
from sklearn.metrics import confusion_matrix, plot_confusion_matrix
import cv2
import random
import itertools
import os

# build confusion matrix, from sklearn
def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')
    print(cm)

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
            horizontalalignment="center",
            color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.show()

# convert images into array with labels
def create_data(path_to_data, array_data):
    for category in CATEGORIES:
        path = os.path.join(path_to_data,category).replace('\\','/')
        class_num = CATEGORIES.index(category)
        for img in tqdm(os.listdir(path)):
            try:
                img_array = cv2.imread(os.path.join(path,img) ,cv2.IMREAD_UNCHANGED)
                new_array = cv2.resize(img_array, (28, 28))
                array_data.append([new_array, class_num])
            except Exception as e:
                print("Error in getting new image")
    return array_data

# input data
data_path_train = 'Shoes/train'
data_path_test = "Shoes/test"
CATEGORIES = ['Flats', 'Heels']
model = tf.keras.models.load_model('model.h5')

img_rows, img_cols, num_channels = 28, 28, 1
num_classes = 2
inputShape = (img_rows, img_cols, num_channels)

training_data = []
testing_data = []

training_data = create_data(data_path_train, training_data)
testing_data = create_data(data_path_test, testing_data)
random.shuffle(training_data)
random.shuffle(testing_data)

x_train = []
y_train = []
x_test = []
y_test = []

# divide data into image data and labels
for features,label in training_data:
    x_train.append(features)
    y_train.append(label)

for features,label in testing_data:
    x_test.append(features)
    y_test.append(label)


x_train = np.array(x_train)
y_train = np.array(y_train)
x_test = np.array(x_test)
y_test = np.array(y_test)


# binary encoding
y_train_one_hot = tf.keras.utils.to_categorical(y_train, 2)
y_test_one_hot = tf.keras.utils.to_categorical(y_test, 2)

# normalize pixels value
x_train = x_train.reshape(x_train.shape[0], img_cols, img_rows, num_channels)/255
x_test = x_test.reshape(x_test.shape[0], img_cols, img_rows,num_channels)/255

# declare which layers is not trainable
for layer in model.layers[:5]:
    layer.trainable = False

# declare new model with pretrained weights
# cut layer on outputs
model = Model(inputs=model.input, outputs=model.get_layer('flatten').output)
x = model.output

#add layer
output = Dense(2, activation='softmax')(x)

#build new model
model = Model(inputs=model.input, outputs=output)
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
model.summary()

# declare data augmentation
gen = ImageDataGenerator(rotation_range=30
                         , width_shift_range=0.25
                         , height_shift_range=0.25
                         , shear_range=0.3
                         , zoom_range=0.3
                         , channel_shift_range=10.
                         , horizontal_flip=True)
# generate train dataset
gen.fit(x_train)

history = model.fit(gen.flow(x_train, y_train_one_hot, batch_size=8)
                    , epochs = 10, verbose=1, batch_size = 5)

# results
predictions = model.predict(x=x_test, steps=len(x_test), verbose=1)

preds = model.evaluate(x = x_test, y = y_test_one_hot)
print('model.evaluate: ',str(preds))

rounded_predictions = np.argmax(predictions, axis= -1)
print('final prediction: ',rounded_predictions)
print('real labels: ', y_test)

cm = confusion_matrix(y_true=y_test, y_pred= rounded_predictions)

plt.plot(history.history['loss'])
plt.title('Loss')
plt.show()

plt.plot(history.history['accuracy'])
plt.title('accuracy')
plt.show()

cm_plot_labels = classes=['Flat', 'Heels']
plot_confusion_matrix(cm=cm, classes=cm_plot_labels, title='Confusion Matrix')
plt.show()