import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten
from tensorflow.keras.layers import Conv2D, MaxPool2D, BatchNormalization
from sklearn.metrics import confusion_matrix, plot_confusion_matrix
import itertools
import numpy as np
import matplotlib.pyplot as plt

# change labels from 0-9 to 0-1
def change_labels(send_array):
    y_arr = [a for a in send_array]
    y_arr = np.array(y_arr)
    for label in range(len(y_arr)):   # 5 7 9
        if y_arr[label] == 5 or y_arr[label] == 7 or y_arr[label] == 9:
            y_arr[label] = 1
        else: y_arr[label] = 0
    return y_arr

# build confusion matrix, from sklearn
def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
            horizontalalignment="center",
            color="white" if cm[i, j] > thresh else "black")
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.show()

# input data parameters
imgRows, imgCols, numChannels = 28, 28, 1
numClasses = 2
inputShape = (imgRows, imgCols, numChannels)

# download data
(x_train, Y_train), (x_test, Y_test) = tf.keras.datasets.fashion_mnist.load_data()

# create validation data
val_lenght = int(len(x_test) * 0.7)
x_val = x_test[:val_lenght]
Y_val = Y_test[:val_lenght]
x_test = x_test[val_lenght:]
Y_test = Y_test[val_lenght:]

# change labels to binary classificator
y_train = change_labels(Y_train)
y_val = change_labels(Y_val)
y_test = change_labels(Y_test)

# rescale pixel value 0-255 to 0-1
x_train = x_train.reshape(x_train.shape[0], imgCols, imgRows,numChannels )/255 # (1- żeby nie musieć pamiętać ile jest przykładów, 2,3 - wielkość obrazka, 4- kanał, 5- wartości wewnatzr tablicy normalizuję)
x_val = x_val.reshape(x_val.shape[0], imgCols, imgRows,numChannels)/255
x_test = x_test.reshape(x_test.shape[0],imgCols, imgRows,numChannels)/255

# convert a class vector to binary class matrix
y_train_one_hot = tf.keras.utils.to_categorical(y_train, 2)
y_val_one_hot = tf.keras.utils.to_categorical(y_val, 2)
y_test_one_hot = tf.keras.utils.to_categorical(y_test, 2)

# structure of model
model = Sequential([
    Conv2D(filters=32, kernel_size=(3, 3), activation=tf.keras.layers.LeakyReLU(alpha=0.1), padding = 'same',
           input_shape=inputShape),
    MaxPool2D(pool_size=(2, 2), strides=2),
    Conv2D(filters=64, kernel_size=(3, 3), activation=tf.keras.layers.LeakyReLU(alpha=0.1), padding = 'same'),
    MaxPool2D(pool_size=(2, 2), strides=2),
    Conv2D(filters=128, kernel_size=(3, 3), activation=tf.keras.layers.LeakyReLU(alpha=0.1), padding='same'),
    MaxPool2D(pool_size=(2, 2), strides=2),
    Flatten(),
    Dense(units=numClasses, activation='softmax')  # add conv2d 128, add dense 100 or 128
])

model.summary()
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

history = model.fit(x = x_train, y = y_train_one_hot, batch_size = 300, epochs = 12,
                    verbose=1, validation_data=(x_val, y_val_one_hot))


# presenting results
preds =  model.evaluate(x = x_test, y = y_test_one_hot)
print('model.evaluate: ',str(preds))

predictions = model.predict(x=x_test, steps=len(x_test), verbose=1)

rounded_predictions = np.argmax(predictions, axis= -1)
print('final prediction: ',rounded_predictions)
print('real labels: ', y_test)

cm = confusion_matrix(y_true=y_test, y_pred= rounded_predictions)

plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.legend(['train', 'val'])
plt.title('Loss')
plt.show()

plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.legend(['train', 'val'])
plt.title('Accuracy')
plt.show()

cm_plot_labels = classes=['not shoes', 'shoes']
plot_confusion_matrix(cm=cm, classes=cm_plot_labels, title='Confusion Matrix')

model.save('model.h5')


